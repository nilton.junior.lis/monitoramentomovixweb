import { useContext, useState, useEffect } from 'react';
import './App.css';
import GlobalState from './context/GlobalContext/GlobalState';
import HomePage from './pages/HomePage';
import Unauthorized from './pages/Unauthorized';
import GlobalStateContext from './context/GlobalContext/GlobalStateContext';

function App() {
  let http = window.location.href.indexOf("?token");
  

  // const sendToken = () => {        
  //     api(config).post('/session', {token: token})
  //     .then((res) => {
  //         localStorage.setItem("@token", res.data.token);
  //     })
  //     .catch((err) => {
  //         console.log(err)
  //     })
  // }

  // useEffect(() => {
  //   if(config!=null && token!=null) 
  //       sendToken()
  // },[config, token])

  const checkHttp = () => {
    if(http != -1){
      return(
        <>
         <HomePage />
        </>
      )
    } else {
        return(
          <>
          <Unauthorized/>
          </>
        )
    }
}

  return (
    <div>
      <GlobalState>
      {checkHttp()}
      </GlobalState>
    </div>
  );
}

export default App;
