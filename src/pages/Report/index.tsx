import { Button } from "primereact/button";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import React, { CSSProperties, useContext, useEffect, useState } from "react";
import { PuffLoader } from "react-spinners";
import { api } from "../../service/api";
import * as S from "./styled"
import { Calendar } from 'primereact/calendar';
import { InputText } from 'primereact/inputtext';
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";


const override: CSSProperties = {
    display: "block",
    margin: "0 auto",
    borderColor: "red",
};

interface ReportProps {
    showError:any 
    showWarn:any
}

const Report : React.FC<ReportProps> = ({showWarn, showError}) => {
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false);
    const [color, setColor] = useState("#085769");
    const [date, setDate] = useState<any>([
        new Date((new Date()).setMonth(((new Date()).getMonth() -1))),
        new Date()
    ]);
    const [type, setType] = useState<any>(0);
    const [filialSelect, setFilialSelect] = useState<any>(0);


    const {regional, filial, regionalSelect, setRegionSelect, config } = useContext<any>(GlobalStateContext);


    const formatDate = (value:string) => {
        const day = new Date(value).getDate()
        const month = new Date(value).getMonth() + 1
        const years = new Date(value).getFullYear()

        return `${day}/${month}/${years} `
    }

    const dateBodyTemplate = (rowData:any) => {
        return formatDate(rowData.dataHoraTransacao);
    }

    let objResponse = { 
        start: date[0], 
        end: date[1], 
        filial: filialSelect,
        regional: regionalSelect, 
        type: type 
    }
    //Requisição para API
    const getDados = () => {
        setLoading(true)
        if(config!=null)
            api(config).post(`/report`, objResponse)
            .then((res) => {
                setData(res.data.data)
                setLoading(false)
                console.log(res.data.data)
            })
            .catch((err) => {
                console.log(err.response.data.code)
                if(err.response.status === 404){
                    showWarn("Sem resultados para essa pesquisa.")
                    setLoading(false)
                    setData([])
                }else if(err.response.data.code === 0){
                    showWarn(err.response.data.message)
                    setLoading(false)
                    setData([])
                }else {
                    setLoading(false)
                    showError("Erro ao fazer sua busca.")
                    setData([])
                }
            })
    }

    
    const paginatorLeft = <Button type="button" icon="pi pi-refresh" className="p-button-text" />;
    const paginatorRight = <Button type="button" icon="pi pi-cloud" className="p-button-text" />;

    return(
        <>
        <S.Body>
            <div style={{position: "absolute", marginTop: 300, zIndex: 10}}>
        <PuffLoader
            color={color}
            loading={loading}
            cssOverride={override}
            size={150}
            aria-label="Loading Spinner"
            data-testid="loader"
        />
        </div>
            <S.CardStreaming>
                <div style={{display:'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: "100%"}}>
                    <S.TextTittle>RELATÓRIOS</S.TextTittle>
                </div>
                <div style={{width: '100%', marginLeft: 50}}>
                <S.SelectTransmision value={type} onChange={(e) => setType(Number(e.target.value))} >
                <S.OptionTransmision>Tipo de Chamado</S.OptionTransmision>
                <S.OptionTransmision value={0}>Chamados Abertos</S.OptionTransmision>
                <S.OptionTransmision value={1}>Chamados Não Transmitidos</S.OptionTransmision>
                </S.SelectTransmision>
                <Calendar style={{height: 40, marginTop: 5}} placeholder="Período" id="range" value={date} onChange={(e:any) => setDate(e.value)} selectionMode="range" dateFormat="dd/mm/yy" readOnlyInput />
                    <S.SelectTransmision value={regionalSelect} onChange={(e: any) => setRegionSelect(e.target.value)}>
                    <S.OptionTransmision>Regional</S.OptionTransmision>
                    {regional?.map((itens:any) => {
                        return(
                            <>
                               <S.OptionTransmision value={itens.id}>{itens.name}</S.OptionTransmision>
                            </>
                        )
                    })}
                    </S.SelectTransmision>
                    <S.SelectTransmision  value={filialSelect} onChange={(e: any) => setFilialSelect(e.target.value)} >
                    <S.OptionTransmision value={-1} >Filial</S.OptionTransmision>
                    {filial?.map((itens:any) => {
                        return(
                            <>
                               <S.OptionTransmision value={itens.id}>{itens.name}</S.OptionTransmision>
                            </>
                        )
                    })}    
                    </S.SelectTransmision>
                    <Button onClick={() => getDados()} color="#085769" icon="pi pi-search" className="p-button-rounded p-button-info"   aria-label="Search" />
                </div>
                <div style={{display:'flex', flexDirection: 'row', width: "100%", justifyContent: 'center', marginTop: 10, marginBottom: 60}}>
                    <DataTable style={{ width:"100%"}} value={data} paginator responsiveLayout="scroll"
                            paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                            currentPageReportTemplate="Showing {first} to {last} of {totalRecords}" rows={10} rowsPerPageOptions={[10,20]}
                            selectionMode="checkbox" dataKey="numrDfq" paginatorLeft={paginatorLeft} paginatorRight={paginatorRight}>
                            <Column field="fechadura" header="Fechadura" style={{ width: '25%' }}></Column>            
                            <Column field="filial" header="Filial" style={{ width: '25%' }}></Column>            
                            <Column field="equipamento" header="Equipamento" style={{ width: '25%' }}></Column>            
                            <Column field="status" header="Status" style={{ width: '25%' }}></Column>            
                            <Column field="dataHoraTransacao" header="Data" body={dateBodyTemplate} style={{ width: '25%' }}></Column>                       
                    </DataTable>
                </div>
            </S.CardStreaming>
        </S.Body>
        </>
    )
}

export default Report;