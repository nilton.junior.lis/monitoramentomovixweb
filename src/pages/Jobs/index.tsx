import React, { CSSProperties, useEffect, useState, useContext } from "react";
import * as S from "./styled"
import { Chart } from "react-google-charts";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { api } from "../../service/api";
import { PuffLoader } from "react-spinners";
import { blob } from "node:stream/consumers";
import { Calendar } from "primereact/calendar";
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";

const override: CSSProperties = {
    display: "block",
    margin: "0 auto",
    borderColor: "red",
};

interface JobsProps {
    showError:any 
    showWarn:any
}


const Jobs:React.FC<JobsProps> = ({showError,showWarn}) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [loadingButton, setLoadingButton] = useState(false);
    const [color, setColor] = useState("#085769");
    const [dates2, setDates2] = useState<any>([
        new Date((new Date()).setMonth(((new Date()).getMonth() -1))),
        new Date()
    ]);
    const [filiaID, setFilialID] = useState<any>(0);
    const [regionalID, setRegionalID] = useState<any>(0);
    const [serviceID, setServiceID] = useState<any>(0);

    const [disableRegional, setDisableRegional] = useState(true);
    const [disableFilial, setDisableFilial] = useState(true);
  

    const {regional, filial,serviceArray, config} = useContext<any>(GlobalStateContext);

    let objData = { 
            region:  parseInt(regionalID) === 0 ? null : parseInt(regionalID),
            subsidiary: parseInt(filiaID)  === 0 ? null : parseInt(filiaID),
            service: parseInt(serviceID)  === 0 ? null : parseInt(serviceID),
            start: dates2[0],
            end: dates2[1]
    }

    const handleClickSearch = () => {
    
        getDadosJobs(objData)
    }

    useEffect(() => {
        if(objData.service != null){
            setDisableRegional(false)
        }else {
            setDisableRegional(true)
            setRegionalID(0)
            setFilialID(0)
        }

        if(objData.region === null){
            setDisableFilial(true)
        }else {
            setDisableFilial(false)
        }
    },[serviceID,regionalID])



    const getDadosJobs = (obj:any) => {
        setLoading(true)
        if(config!=null)
            api(config).post('/jobs', obj)
                .then((res) => {
                    setData(res.data.data)
                    setLoading(false)
                })
                .catch((err) => {
                    console.log(err.response.data.code)
                    if(err.response.status === 404){
                        showWarn("Sem resultados para essa pesquisa.")
                        setLoading(false)
                        setData([])
                    }else if(err.response.data.code === 0){
                        showWarn(err.response.data.message)
                        setLoading(false)
                        setData([])
                    }else {
                        setLoading(false)
                        showError("Erro ao fazer sua busca.")
                        setData([])
                    }
                })
    }

    const downloadDadosJob = () => {
        setLoadingButton(true)
        if(config!=null)
            api(config).post('/jobs/export', objData ,{responseType: "blob"})
            .then((res) => {
                const href = URL.createObjectURL(res.data);


                const link = document.createElement('a');
                link.href = href;
                link.setAttribute('download', 'file.csv'); //or any other extension
                document.body.appendChild(link);
                link.click();
            
                // clean up "a" element & remove ObjectURL
                document.body.removeChild(link);
                URL.revokeObjectURL(href);

                setLoadingButton(false)
            })
            .catch((err) => {
                setLoadingButton(false)
                showError()
            })
    }

    useEffect(() => {
        getDadosJobs(objData)
    },[config])


    const formatDate = (value:string) => {
        const day = new Date(value).getDate()
        const month = new Date(value).getMonth() + 1
        const years = new Date(value).getFullYear()

        const hour = new Date(value).getHours()
        const minutes = new Date(value).getMinutes()

        return `${day}/${month}/${years}  ${hour}:${minutes}`
    }

    const dateBodyTemplate = (rowData:any) => {
        return formatDate(rowData.measurement);
    }
    
    const typeBodyTemplate = (rowData:any) => {
        if(rowData === 0){
            return "Chamado não transmitido"
        }else {
            return "Abertura de chamados"
        }
    }

    const paginatorLeft = <Button type="button" icon="pi pi-refresh" className="p-button-text" />;
    const paginatorRight = <Button type="button" icon="pi pi-cloud" className="p-button-text" />;


    return (
        <S.Body>
            <S.CardStreaming>
                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: "100%" }}>
                    <S.TextTittle>JOBS</S.TextTittle>
                </div>
                <div style={{ display: 'flex',  flexDirection: 'row', width: "100%", justifyContent: 'center', marginTop: 30, marginBottom: 20 }}>
                <S.SelectTransmision style={{marginLeft: 10}} value={serviceID} onChange={(e) => setServiceID(e.currentTarget.value)}>
                    <S.OptionTransmision value={0}>Services</S.OptionTransmision>
                    {serviceArray()?.map((itens:any) => {
                        return(
                            <>
                               <S.OptionTransmision value={itens.id}>{itens.name}</S.OptionTransmision>
                            </>
                        )
                    })}
                </S.SelectTransmision>
                <Calendar style={{height: 40, marginTop: 5}} placeholder="Período" id="range" value={dates2} onChange={(e:any) => setDates2(e.value)} selectionMode="range" readOnlyInput dateFormat="dd/mm/yy" />
                <S.SelectTransmision disabled={disableRegional} style={{marginLeft: 10}} value={regionalID} onChange={(e) => setRegionalID(e.currentTarget.value)}>
                    <S.OptionTransmision value={0}>Regional</S.OptionTransmision>
                    {regional?.map((itens:any) => {
                        return(
                            <>
                               <S.OptionTransmision value={itens.coD_REGIONAL}>{itens.noM_REGIONAL}</S.OptionTransmision>
                            </>
                        )
                    })}
                </S.SelectTransmision>
                <S.SelectTransmision disabled={disableFilial} value={filiaID} onChange={(e) => setFilialID(e.target.value)}>
                <S.OptionTransmision >Filial</S.OptionTransmision>
                    {filial?.map((itens:any) => {
                        return(
                            <>
                               <S.OptionTransmision value={itens.coD_FILIAL}>{itens.noM_FILIAL}</S.OptionTransmision>
                            </>
                        )
                    })}
                </S.SelectTransmision>
                <Button onClick={() => handleClickSearch()} color="#085769" icon="pi pi-search" className="p-button-rounded p-button-info"   aria-label="Search" />
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', width: "100%", justifyContent: 'center', marginTop: 30, marginBottom: 20 }}>
                <div style={{ position: "absolute", justifySelf:"center", alignSelf:"center", zIndex: 10 }}>
                <PuffLoader
                    color={color}
                    loading={loading}
                    cssOverride={override}
                    size={150}
                    aria-label="Loading Spinner"
                    data-testid="loader"
                />
                </div>
                    <DataTable style={{ width: "100%" }} value={data} paginator responsiveLayout="scroll"
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords}" rows={10} rowsPerPageOptions={[10, 20]}
                        selectionMode="checkbox" dataKey="numrDfq" paginatorLeft={paginatorLeft} paginatorRight={paginatorRight}>
                        <Column field="measurement" header="Data" dataType="date" style={{ width: '25%' }} body={dateBodyTemplate}></Column>
                        <Column field="service.name" header="Nome" style={{ width: '25%' }}></Column>
                        <Column field="service.type" header="Tipo"  body={typeBodyTemplate} style={{ width: '25%' }}></Column>
                    </DataTable>
                </div>
                <Button loading={loadingButton} onClick={() => downloadDadosJob()} style={{marginBottom: 40}} label="Donwload do histórico" icon="pi pi-bookmark" />
            </S.CardStreaming>
        </S.Body>
    )
}

export default Jobs;