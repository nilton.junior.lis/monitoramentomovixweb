import React, { useEffect, useState, useContext } from "react";
import * as S from "./styled";
import logo from '../../assets/imgs/brinkslogo.png';
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";

const Unauthorized:React.FC = () => {
    const {config, isAuth} = useContext<any>(GlobalStateContext);
    
    useEffect(() => {      
        setTimeout(() => {
          if(!!config && !isAuth)
            window.location.href = config.REACT_APP_HTTP_BAS
        }, 5000)
    },[config])

    return(
        <S.Body>
            <S.Logo src={logo} ></S.Logo>
            <div style={{display:'flex',flexDirection: "column"}}>
            <S.TextBody>Não Autorizado</S.TextBody>
            <S.TextSubBody>Você sera redirecionado.</S.TextSubBody>
            </div>
        </S.Body>
    )
}

export default Unauthorized;