import styled from "styled-components";




export const Body = styled.div`
    height: 100vh;
    width: 100%;
    background-color: #085769;
    display: flex;
    flex-direction: column;
    align-items: center;

`

export const TextBody = styled.text`
    color: white;
    font-size: 80px;
    font-weight: 700;
    margin-top: 100px;


`
export const TextSubBody = styled.text`
    color: white;
    font-size: 20px;



`


export const Logo = styled.img`
    padding: 30px;
    background-color: white;
    border-radius: 10px;
    margin-top: 50px;
    height: 120px;
`