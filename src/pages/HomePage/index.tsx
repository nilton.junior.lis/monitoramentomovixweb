import React, { useEffect, useRef, useState, useContext } from "react";
import Menu from "../../components/Menu";
import DashBoard from "../DashBoard";
import Jobs from "../Jobs";
import Report from "../Report";
import Streaming from "../Streaming";
import Transmission from "../Transmission";
import * as S from "./styled"
import { Toast } from 'primereact/toast';
import Steps from "../Steps";
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";
import Unauthorized from "../Unauthorized";
import { api } from "../../service/api";
import Auth from "../Auth";




const HomePage : React.FC = ({}) => {
    const [selectOption, setSelectOption] = useState<number>(1)
    const [tokenAPI, setTokenAPI] = useState<string>('')

    const {token, isAuth, inAuth, config} = useContext<any>(GlobalStateContext);

    const toast = useRef<any>();

        const showSuccess = (msg:string) => {
            toast.current.show({severity:'success', summary: 'Success Message', detail:msg, life: 3000});
        }

        const showError = (msg:string) => {
            toast.current.show({severity:'error', summary: 'Erro', detail:msg , life: 3000});
        }

        const showWarn = (msg:string) => {
            toast.current.show({severity:'warn', summary: 'Warning', detail:msg , life: 3000});
        } 


    const chooseScreen = () => {
        if(selectOption === 1){
            return(
                <>
                <Streaming selectOption={selectOption} showError={showError}/>
                </>
            )
        } else if(selectOption === 2) {
            return(
                <Jobs showError={showError} showWarn={showWarn}/>
            )
        }else if(selectOption === 3){
            return(
                <>
                <Transmission showSuccess={showSuccess}  showError={showError} />
                </>
            )
        } else if(selectOption === 4){
            return(
                <>
                    <Report showWarn={showWarn}  showError={showError} />
                </>
            )
        } else if(selectOption === 5){
            return(
                <>
                    <DashBoard/>
                </>
            )
        } else {
            return(
                <>
                    <Steps  showError={showError} showWarn={showWarn}/>
                </>
            )
        }

    }

    return(
        <S.Body>
            {inAuth ? <Auth/> :
                !isAuth ? <Unauthorized/> :
                    <>
                        <Toast ref={toast} />
                        <Menu 
                            setSelectOption={setSelectOption} 
                            selectOption={selectOption}/>
                        {chooseScreen()}
                    </>
            }     
        </S.Body>
    )
}

export default HomePage;