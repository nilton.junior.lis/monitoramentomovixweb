import { CSSProperties, useEffect, useState, useContext } from "react";
import ButtonPrimary from "../../components/ButtonPrimary";
import * as S from "./styled"
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Button } from "primereact/button";
import { api } from "../../service/api";
import { PuffLoader } from "react-spinners";
import { Calendar } from "primereact/calendar";
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";


const override: CSSProperties = {
    display: "block",
    margin: "0 auto",
    borderColor: "red",
};

interface StepsProps {
    showError: any
    showWarn: any
}

const Steps: React.FC<StepsProps> = ({ showError, showWarn }) => {
    const [email, setEmail] = useState<string>("")
    const [value2, setValue2] = useState('');
    const [data, setData] = useState([]);
    const [obgSelectProc, setObgSelectProc] = useState<any>([])
    const [color, setColor] = useState("#085769");
    const [loading, setLoading] = useState(false);
    const [dates2, setDates2] = useState<any>([
        new Date((new Date()).setMonth(((new Date()).getMonth() -1))),
        new Date()
    ]);
 
    const { config } = useContext<any>(GlobalStateContext);

    const [openModal, setOpenModal] = useState<boolean>(false);

    let objButton = {
        start: dates2[0],
        end: dates2[1]
     }


    const typeBodyTemplate = (rowData: any) => {

        let data: any[] = rowData.fails

        return (
            <>
                <text>{data.length}</text>
            </>
        )
    }
    const typeBodyTemplateTotal = (rowData: any) => {

        let data: any[] = rowData.fails

        return (
            <>
                <text>{data.length}</text>
            </>
        )
    }

    const percentageBodyTemplate = (rowData: any) => {



        return (
            <>
                <text>{rowData.percent.toFixed(2)}%</text>
            </>
        )


    }

    const textBodyTemplate = (rowData: any) => {

        return (
            <>
                <text>{obgSelectProc[0].subsidiary}</text>
            </>
        )
    }
    const textRegionalBodyTemplate = (rowData: any) => {

        return (
            <>
                <text>{obgSelectProc[0].regional}</text>
            </>
        )
    }

    const handleButtonPress = () => {
        if (dates2 === null) {
            showWarn("Selecione um período para realizar a busca.")
        } else {
            getData(objButton)
        }

    }

    const getData = (obj:any) => {
        setLoading(true)
        if(config!=null)
            api(config).post("/steps", obj)
                .then((res) => {
                    setData(res.data.data)
                    setLoading(false)
                })
                .catch((err) => {
                    if (err.response.status === 404) {
                        showWarn("Sem resultados para essa pesquisa.")
                        setLoading(false)
                        setData([])
                    } else {
                        setLoading(false)
                        showError("Erro ao fazer sua busca.")
                        setData([])
                    }
                })
    }

    useEffect(() => {
        getData(objButton)
    },[config])

    const paginatorLeft = <Button type="button" icon="pi pi-refresh" className="p-button-text" />;
    const paginatorRight = <Button type="button" icon="pi pi-cloud" className="p-button-text" />;


    return (
        <S.Body>
            <PuffLoader
                color={color}
                style={{ position: "absolute", zIndex: 10, top: 200 }}
                loading={loading}
                cssOverride={override}
                size={150}
                aria-label="Loading Spinner"
                data-testid="loader"
            />
            <div style={{ marginBottom: 30, marginLeft: 30, alignSelf: 'flex-start' }}>
                <Calendar style={{ height: 40, marginRight: 10 }} placeholder="Período" id="range" value={dates2} onChange={(e: any) => setDates2(e.value)} dateFormat="dd/mm/yy" selectionMode="range" readOnlyInput />
                <Button onClick={() => handleButtonPress()} color="#085769" icon="pi pi-search" className="p-button-rounded p-button-info" aria-label="Search" />
            </div>
            <div style={{ display: 'flex', flexDirection: "row" }}>
                <S.CardStreaming style={{ height: "80%", width: "45%" }}>
                    <DataTable style={{ height: "" }} value={data} paginator responsiveLayout="scroll"
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords}" rows={10} rowsPerPageOptions={[10, 20]}
                        selectionMode="checkbox" dataKey="numrDfq" paginatorLeft={paginatorLeft} paginatorRight={paginatorRight} onSelectionChange={e => setObgSelectProc(e.value)}>
                        <Column field="regional" header="Regional" style={{ width: '25%' }}></Column>
                        <Column field="subsidiary" header="Filial" style={{ width: '25%' }}></Column>
                        <Column field="totalTickets" header="Total" style={{ width: '25%' }}></Column>
                        <Column field="fails" body={typeBodyTemplate} header="Quantidade de Falhas" style={{ width: '25%' }}></Column>
                        <Column field="percent" body={percentageBodyTemplate} header="% Falhas da filial" style={{ width: '25%' }}></Column>
                    </DataTable>
                </S.CardStreaming>
                <S.CardStreaming style={{ height: "50%", width: "45%" }}>
                    <DataTable style={{ height: "" }} value={obgSelectProc[0]?.fails} paginator responsiveLayout="scroll"
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords}" rows={10} rowsPerPageOptions={[10, 20]}
                        selectionMode="checkbox" dataKey="numrDfq" paginatorLeft={paginatorLeft} paginatorRight={paginatorRight}>
                        <Column body={textRegionalBodyTemplate} header="Regional" style={{ width: '25%' }}></Column>
                        <Column body={textBodyTemplate} header="Filial" style={{ width: '25%' }}></Column>
                        <Column field="description" header="Qual Falha?" style={{ width: '25%' }}></Column>
                    </DataTable>
                </S.CardStreaming>
                {openModal === true ?
                    <S.Modal>
                        <S.TextTittle>Corpo do E-mail</S.TextTittle>
                        <InputTextarea value={value2} onChange={(e) => setValue2(e.target.value)} style={{ width: '90%', overflowY: 'auto' }} rows={10} cols={30} autoResize />
                        <div>
                            <ButtonPrimary functionClick={() => setOpenModal(false)} style={{ marginTop: 20, marginLeft: 10, marginBottom: 20 }} text={'Fechar'}></ButtonPrimary>
                        </div>
                    </S.Modal> : null
                }
            </div>
        </S.Body>
    )
}

export default Steps;