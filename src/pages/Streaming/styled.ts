import styled from "styled-components";



export const Body = styled.div`
    width:  80%;
    display: flex;
    flex-direction: column;
    align-items: center;
`
export const CardStreaming = styled.div`
    width: 80%;
    background-color: white;
    box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    margin-top: 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 50px;
`
export const SubCardStreaming = styled.div`
    width: 90%;
    background-color: #EAEAEA;
    border-radius: 10px;
    margin-top: 20px;
    display: flexbox;
    margin-bottom: 30px;
    flex-wrap: wrap;

`

export const TextTittle = styled.text`
    font-style: normal;
    font-weight: 600;
    font-size: 40px;
    line-height: 73px;
    color: #00539B;

`

export const FirstBall = styled.div`
    height: 25px;
    width: 25px;
    border-radius: 20px;
    background-color: #49DA25;
    cursor: pointer;
`
export const SecondBall = styled.div`
    height: 10px;
    width: 10px;
    border-radius: 20px;
    align-self: flex-end;
`

export const SubTittle = styled.text`


`

export const ModalInfos = styled.div`
    position: absolute;
    z-index: 10;
    align-self: center;
    justify-self: center;
    background-color: white;
    margin-top: 25%;
    display: flex;
    flex-direction: column;


`

export const SelectTransmision = styled.select`
    border: 1px solid #ced4da;
    height: 40px;
    border-radius: 8px;
    margin: 5px;
    font-size: 15px;
    color: #00539B;
    font-weight: 600;

`

export const OptionTransmision = styled.option`
    font-size: 15px;

`