import React, { useEffect, useState, useContext } from "react";
import { PuffLoader } from "react-spinners";
import { CSSProperties } from "styled-components";
import { api } from "../../service/api";
import * as S from "./styled"
import { ModalHover } from 'react-modal-hover';
import Modal from 'react-modal';
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";

interface StreamingProps {
    showError: any
    selectOption: number

}

interface SubMenuObj {
    sigla: string,
    statusMaster: number,
    subsSatus: number

}

const override: CSSProperties = {
    display: "block",
    margin: "0 auto",
    borderColor: "red",
};





const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };


const Streaming: React.FC<StreamingProps> = ({ showError , selectOption}) => {
    const [loading, setLoading] = useState(false);
    const [nameFirstRegion, setNameFirstRegion] = useState('')
    const [regions, setRegions] = useState([]);
    const [regionsGlobal, setRegionsGlobal] = useState<any>([]);
    const [openModal, setOpenModal] = useState(false);
    const [itensSelect, setItenSelect] = useState<any>(null);
    const [selectRegion, setSelectRegion] = useState(false);
    const [jobsSelect,setJobsSelect] = useState<any>();
    const [seconds, setSeconds] = useState(0);
    const [stop, setStop] = useState(true);
    const {config} = useContext<any>(GlobalStateContext);

    let subtitle:any;

    const checkStatus = (status: any) => {
        if (status === true) {
            return '#49DA25'
        } else if (status === 2) {
            return "#FAFF00"
        } else {
            return "grey"
        }
    
    }

    const getInfosStreaming = () => {
        if(config!=null){
            setLoading(true)
            api(config).get('/Monitoring')
                .then((res) => {
                    setRegions(res.data.data.regions)
                    setRegionsGlobal(res.data.data.global.jobs)
                    setNameFirstRegion(res.data.data.global.name)
                    setLoading(false)
                })
                .catch((err) => {
                    setLoading(false)
                    console.log(err)
                    showError()
                })
        }
    }

    const clickSelectIten = (item:any) => {
        setItenSelect(item)
        setOpenModal(true)
        setSelectRegion(false)
    }
    const clickSelectItenRegions = (item:any) => {
        setItenSelect(item)
        setOpenModal(true)
        setSelectRegion(true)
    }

    const closeModal = () => {
        setOpenModal(false)
    }

    function afterOpenModal() {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#f00';
      }

    const fomartData = (data:string) => {
        const day = new Date(data).getDate();
        const month = new Date(data).getMonth() + 1;
        const year = new Date(data).getFullYear();        
        return `${day}/${month.toString().length === 1 ? `0${month}` : month}/${year}`
    }

    const componentInterval = () => {
        setTimeout(() => {
          setSeconds((prevState) => prevState + 1);
          getInfosStreaming()
        }, 30000);
      };

    useEffect(() => {
        getInfosStreaming()
        setStop(false)
    }, [config])

    useEffect(() => {
        if(selectOption != 1){
            setStop(true)
        }
    }, [selectOption])

    useEffect(() => {
        componentInterval();
      }, [stop === true ? null : seconds]);

    const checkjobsStatus = (array:any) => {
        let newArray:any[] = [];

        array.map((itens:any) => {
            if(itens.status === false){
                newArray.push(itens)
            } else {

            }
        })

        if(newArray.length >= 2){
            return "grey"
        } else if(newArray.length === 1){
            return "#FAFF00"
        } else {
            return "#49DA25"
        }

    }

    return (
        <S.Body>
            <S.TextTittle>Monitoramento</S.TextTittle>
            <Modal
        isOpen={openModal}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        {selectRegion === false ? 
                <div style={{display: "flex", flexDirection: "column", justifyContent:"center", alignItems: "center"}}>
                <text style={{fontSize: 20}}>{itensSelect?.name}</text>
                <text style={{fontSize: 15, marginBottom: 20}}>{fomartData(itensSelect?.measurement)}</text>
                <text>{itensSelect?.metadata.description}</text>
                </div> : 
                <div style={{display: "flex", flexDirection: "column", justifyContent:"center", alignItems: "center"}}>
                <text style={{fontSize: 20}}>{itensSelect?.name}</text>
                <S.SelectTransmision value={jobsSelect} onChange={(e) => setJobsSelect(e.target.value)}>
                <S.OptionTransmision value={""}>Selecione</S.OptionTransmision>
                    {itensSelect?.jobs.map((itens:any) => {
                        return(
                            <>
                            <S.OptionTransmision value={itens?.metadata.description} >{itens?.metadata.name}</S.OptionTransmision>
                            </>
                        )
                    })}
                </S.SelectTransmision>
                {jobsSelect ? <text style={{marginTop: 10}}>{jobsSelect}</text> : null}
                </div>
    } 

      </Modal>
            <S.CardStreaming>
                <>
                    <text>{nameFirstRegion}</text>
                    <S.SubCardStreaming>
                        <div style={{position: "absolute", zIndex: 10}}>
                        <PuffLoader
                            color='#085769'
                            loading={loading}
                            cssOverride={override}
                            size={150}
                            aria-label="Loading Spinner"
                            data-testid="loader"
                        />
                        </div>
                        {regionsGlobal?.map((itens: any) => {
                            return (
                                <div style={{ display: "flex", flexDirection: "column", alignItems: 'center', width: "20%"}}>
                                    <div style={{ display: 'flex', marginTop: 10, marginLeft: 10, marginRight: 10, height: '20%' }}>
                                        <S.FirstBall onClick={()=> clickSelectIten(itens)} style={{ backgroundColor: checkStatus(itens.status) }}>
                                        </S.FirstBall>
                                    </div>
                                    <text style={{ marginTop: 10, marginRight: 10, marginLeft: 10, fontSize:10, textAlign: "center" }}>{itens?.name}</text>
                                </div>
                            )
                        })}

                    </S.SubCardStreaming>
                </>
                
                {regions.map((itens: any) => {
                    return (
                        <>
                            <text>{itens.name}</text>
                            <S.SubCardStreaming>
                            <div style={{position: "absolute", zIndex: 10}}>
                                <PuffLoader
                                    color='#085769'
                                    loading={loading}
                                    cssOverride={override}
                                    size={150}
                                    aria-label="Loading Spinner"
                                    data-testid="loader"
                                />
                                </div>
                                {itens.subsidiaries.map((itens: any) => {
                                    return (
                                        <>
                                            <div style={{ display: "flex", flexDirection: "column", alignItems: 'center' }}>
                                                <div style={{ display: 'flex', marginTop: 10, marginLeft: 10, marginRight: 10, height: '20%' }}>
                                                    <S.FirstBall onClick={()=> clickSelectItenRegions(itens)} style={{ backgroundColor: checkjobsStatus(itens.jobs) }}>
                                                    </S.FirstBall>
                                                    {/* <S.SecondBall style={{ backgroundColor: checkStatus("lele") }}>
                                                    </S.SecondBall> */}
                                                </div>
                                                <text style={{ marginTop: 10, marginRight: 10, marginLeft: 10 }}>{itens?.initials}</text>
                                            </div>
                                        </>
                                    )
                                })}
                            </S.SubCardStreaming>
                        </>
                    )
                })}



            </S.CardStreaming>
        </S.Body>
    )
}

export default Streaming;