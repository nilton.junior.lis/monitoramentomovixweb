import React, { useEffect, useState, useContext } from "react";
import * as S from "./styled"
import { Chart } from 'primereact/chart';
import { api } from "../../service/api";
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";

const DashBoard = () => {
    const [topData, setTopData] = useState([])
    const [labels, setLabels] = useState([])
    const [percentage, setPercentage] = useState([])
    const {config} = useContext<any>(GlobalStateContext);


    let Data = {
        labels: labels,
        datasets: [
            {
                label: 'Analise por mês',
                data: percentage,
                fill: false,
                borderColor: '#42A5F5',
                tension: .4
            },
        ]
    };

    let horizontalOptions = {
        indexAxis: 'y',
        maintainAspectRatio: false,
        aspectRatio: .20,
        plugins: {
            legend: {
                labels: {
                    color: '#495057'
                }
            }
        },
        scales: {
            x: {
                ticks: {
                    color: '#495057'
                },
                grid: {
                    color: '#ebedef'
                }
            },
            y: {
                ticks: {
                    color: '#495057'
                },
                grid: {
                    color: '#ebedef'
                }
            }
        }
    };


    const handleDashBoard = () => {        
        api(config).post('/Dashboard',{
            start: "2022-11-05T20:53:29.014Z",
            end: "2022-12-05T20:53:29.014Z",
            regional: 1
        })
        .then((res) => {
            setTopData(res.data.data.top)
            console.log(res.data.data)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    useEffect(() => {
        if(config!=null)
            handleDashBoard()
    },[config])

    useEffect(() => {
        let newArraySigla:any = []
        let newArrayPercentage:any = []

        topData.map((itens:any) => {
            if(itens.sigla === "totalNIO"){
                
            }else {
                newArraySigla.push(itens.sigla)
                newArrayPercentage.push(itens.percentage)
            }
     
        })
        setLabels(newArraySigla)
        setPercentage(newArrayPercentage)
    },[topData])

    return (
        <S.Body>
            <S.CardStreaming>
                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: "100%" }}>
                    <S.TextTittle>DASHBOARD GERAL</S.TextTittle>
                    <S.SelectTransmision style={{ width: 100, marginLeft: 40 }}>
                        <S.OptionTransmision>MÊS</S.OptionTransmision>
                    </S.SelectTransmision>
                    <S.SelectTransmision style={{ width: 100, marginLeft: 10 }}>
                        <S.OptionTransmision>REGIONAL</S.OptionTransmision>
                    </S.SelectTransmision>
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', width: "100%" }}>
                    <div style={{ display: "flex", flexDirection: "row", width: "100%" }}>
                    <div style={{ marginTop: 50 , marginLeft:30}}>
                        <Chart
                            type="line"
                            data={Data}
                            style={{width: "400px"}}
                        />
                        </div>
                        <div style={{ marginTop: 30 , overflowY: 'scroll', height: 300, marginLeft: 30}}>
                        <Chart type="bar" data={Data} options={horizontalOptions} style={{ width: 200}} />
                        </div>
                    </div>
                    <div style={{ display: "flex", flexDirection: "column", width: "50%" }}>
                        <S.SelectTransmision style={{ width: 100, marginLeft:25}}>
                            <S.OptionTransmision>ANO</S.OptionTransmision>
                        </S.SelectTransmision>
                        <S.CardTransmission>
                            <S.NumberTittle>96%</S.NumberTittle>
                            <S.TextSubsTittle>Abertura Movix</S.TextSubsTittle>
                        </S.CardTransmission>
                        <S.CardTransmission>
                            <S.NumberTittle>7,33%</S.NumberTittle>
                            <S.TextSubsTittle>Abertura NIO</S.TextSubsTittle>
                        </S.CardTransmission>
                        <S.CardTransmission>
                            <S.NumberTittle style={{fontSize: 25}}>705,303mil</S.NumberTittle>
                            <S.TextSubsTittle>Total Aberturas</S.TextSubsTittle>
                        </S.CardTransmission>
                        
                    </div>
                </div>
            </S.CardStreaming>
        </S.Body>
    )
}

export default DashBoard;