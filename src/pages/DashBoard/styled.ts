import styled from "styled-components";



export const Body = styled.div`
    height: 100%;
    width:  80%;
    display: flex;
    flex-direction: column;
    align-items: center;
`
export const CardStreaming = styled.div`
    height: 500px;
    width: 80%;
    background-color: white;
    box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    margin-top: 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
`
export const SubCardStreaming = styled.div`
    height: 25%;
    width: 90%;
    background-color: #EAEAEA;
    border-radius: 10px;
    margin-top: 30px;
    display: flexbox;
`

export const TextTittle = styled.text`
    font-style: normal;
    font-weight: 600;
    font-size: 30px;
    line-height: 73px;
    color: #00539B;

`

export const FirstBall = styled.div`
    height: 25px;
    width: 25px;
    border-radius: 20px;
    background-color: #49DA25;

`
export const SecondBall = styled.div`
    height: 10px;
    width: 10px;
    border-radius: 20px;
    align-self: flex-end;
`


export const SelectTransmision = styled.select`
    border: 1px solid #00539B;
    height: 40px;
    border-radius: 8px;
    margin: 5px;
    font-size: 13px;
    color: #00539B;
    font-weight: 600;

`
export const CardTransmission = styled.div`
    border: 1px solid #00539B;
    height: 70px;
    border-radius: 8px;
    margin: 5px;
    width: 80%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

export const OptionTransmision = styled.option`
    font-size: 15px;

`

export const NumberTittle = styled.text`
    font-size: 30px;
    font-weight: 600;
    color: #00539B;
`
export const TextSubsTittle = styled.text`
    font-size: 15px;

`