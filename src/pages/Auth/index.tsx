import React, { useEffect, useState, useContext } from "react";
import * as S from "./styled";
import logo from '../../assets/imgs/brinkslogo.png';
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";
import { api } from "../../service/api";
import { useLocation } from "react-router-dom";

const Auth:React.FC = () => {
    const {config, setInAuth, setIsAuth} = useContext<any>(GlobalStateContext);    
    const session = () => {        
        const params = new URLSearchParams(window.location.search)        
        setInAuth(true);
        api(config).post('/session', {token: params.get('token')})
        .then((res) => {
            setInAuth(false);
            setIsAuth(true);
            localStorage.setItem("@token", res.data.token);
        })
        .catch((err) => {
            setInAuth(false);
            setIsAuth(false);
            console.log(err)
        })
    }    
    useEffect(() => {        
        if(config!=null) {
            session();
        }
    },[config])

    return(
        <S.Body>
            <S.Logo src={logo} ></S.Logo>
            <div style={{display:'flex',flexDirection: "column"}}>
            <S.TextBody>Em autenticação do usuario!</S.TextBody>
            <S.TextSubBody>Você esta em autenticação, em breve o sistema estara pronto.</S.TextSubBody>
            </div>
        </S.Body>
    )
}

export default Auth;