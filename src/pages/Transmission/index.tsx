import { useContext, useState, useEffect, CSSProperties } from "react";
import ButtonPrimary from "../../components/ButtonPrimary";
import * as S from "./styled"
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import GlobalStateContext from "../../context/GlobalContext/GlobalStateContext";
import { SpeedDial } from 'primereact/speeddial';
import { Button } from "primereact/button";
import { api } from "../../service/api";
import { elements } from "chart.js";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { PuffLoader } from "react-spinners";


interface TransmissionProps {
    showError: any
    showSuccess: any
}


const Transmission: React.FC<TransmissionProps> = ({ showError, showSuccess }) => {
    const [value, setValue] = useState<any>();
    const [email, setEmail] = useState<string>("");
    const [assunto, setAssunto] = useState<string>('')
    const [value2, setValue2] = useState('');
    const [registerEmails, setRregisterEmails] = useState<any>([]);
    const [users, setUsers] = useState<any>([]);
    const [calledStream, setCalledStream] = useState<any>([]);
    const [obgSelectProc, setObgSelectProc] = useState<any[]>([]);
    const [filialSelect, setFilialSelect] = useState<any>(-1);
    const [loading, setLoading] = useState(false);

    const [openModal, setOpenModal] = useState<boolean>(false);

    const { regional, filial, setRegionSelect, regionalSelect, config } = useContext<any>(GlobalStateContext);

    let objSend:any = {
        cod_Func: parseInt(value),
        id_chamado:[(obgSelectProc[0]?.iD_CHAMADO)],
        assunto: assunto,
        corpo_email: email
    };


    const override: CSSProperties = {
        display: "block",
        margin: "0 auto",
        borderColor: "red",
    };


    const paginatorLeft = <Button type="button" icon="pi pi-refresh" className="p-button-text" />;
    const paginatorRight = <Button type="button" icon="pi pi-cloud" className="p-button-text" />;

    // const handleRemoveItem = (name: any) => {
    //     setRregisterEmails(registerEmails.filter((item: any) => item !== name))
    // }


    const getCalledStream = () => {
        setLoading(true)
        if(config!=null)
            api(config).post('/CalledStream/Transmission', {
                dT_ATENDIMENTO: null,
                dT_ATENDIMENTO_FIM: null
            })
            .then((res) => {
                setCalledStream(res.data.data)
                setLoading(false)
            })
            .catch((err) => {
                console.log(err)
                setLoading(false)
            })
    }

    const getDados = () => {
        if(config!=null)
            api(config).post('/Collaborator/Info', { id_Collaborator: 0})
            .then((res) => {
                setUsers(res.data)
                console.log("aqui", res.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }

    const sendEmail = () => {
        console.log(objSend)
        setLoading(true)
        if(config!=null)
            api(config).post('/CalledStream/SettingSend', objSend)
            .then((res) => {
            setLoading(false)
            showSuccess("E-mail enviado com sucesso.")
            })
            .catch((err) => {
            setLoading(false)
            console.log(err)
            })
    }

    const filterUsers = () => {
       let result:any[] =  users?.filter((elements: { coD_REGIONAL: number; }) => elements.coD_REGIONAL == parseInt(regionalSelect))

       if(filialSelect != -1){
        let resultFilial:any[] =  result?.filter((elements: { coD_FILIAL: number; }) => elements.coD_FILIAL == parseInt(filialSelect))
        return resultFilial
       } else {
        return result
       }
    
    }

    // const checkSame = (item: any) => {
    //     if (registerEmails.find((element: any) => element === item)) {
    //         showError("E-mail ja digitado.")

    //     } else if (email === "") {
    //         showError("Nenhum e-mail digitado.")
    //     } else {
    //         setRregisterEmails([...registerEmails, email])
    //         setEmail("")
    //     }
    // }

    useEffect(() => {
        getDados()
        getCalledStream()
    },[config])

    useEffect(() => {
        filterUsers()
    },[regionalSelect])
    return (
        <S.Body>
            <div style={{position:'absolute', zIndex: 10 }}>
            <PuffLoader
                            color='#085769'
                            loading={loading}
                            cssOverride={override}
                            size={150}
                            aria-label="Loading Spinner"
                            data-testid="loader"
                        />
            </div>
            <S.CardStreaming style={{ height: "80%", width: "50%" }}>
                <S.TextTittle>Chamados não transmitidos</S.TextTittle>
                <DataTable style={{ width:"90%" }} value={calledStream} paginator responsiveLayout="scroll"
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords}" rows={10} rowsPerPageOptions={[10, 20]}
                        selectionMode="checkbox" dataKey="numrDfq" paginatorLeft={paginatorLeft} paginatorRight={paginatorRight} onSelectionChange={e => setObgSelectProc(e.value)}>
                        <Column field="iD_CHAMADO" header="Id do chamado" style={{ width: '25%' }}></Column>
                        <Column field="noM_ABVD_FILIAL" header="Filial" style={{ width: '25%' }}></Column>
                        <Column field="deS_PROBLEMACliente" header="Descrição" style={{ width: '25%' }}></Column>
                    </DataTable>
            </S.CardStreaming>
            <S.CardStreaming style={{ height: "50%", width: "40%" }}>
                <S.TextTittle>Transmissão</S.TextTittle>
                {obgSelectProc.length === 0 ?  null : <S.TextSubTittle>Chamado número: {obgSelectProc[0]?.iD_CHAMADO }</S.TextSubTittle>}
                <div style={{ width: "80%", display: 'flex', justifyContent: 'center' }}>
                    <S.SelectTransmision value={regionalSelect} onChange={(e: any) => setRegionSelect(e.target.value)} style={{ width: '50%' }}>
                        <S.OptionTransmision >SELECIONAR REGIONAL</S.OptionTransmision>
                        {regional?.map((itens: any) => {
                            return (
                                <S.OptionTransmision value={itens.id}>{itens.name}</S.OptionTransmision>
                            )
                        })}
                    </S.SelectTransmision>
                    <S.SelectTransmision value={filialSelect} onChange={(e:any) => setFilialSelect(e.target.value)} style={{ width: '50%' }}>
                        <S.OptionTransmision value={-1}>SELECIONAR FILIAL</S.OptionTransmision>
                        {filial?.map((itens: any) => {
                            return (
                                <S.OptionTransmision value={itens.id}>{itens.name}</S.OptionTransmision>
                            )
                        })}
                    </S.SelectTransmision>
                </div>
                <S.SelectTransmision value={value} onChange={(e: any) => setValue(e.target.value)} style={{ width: '75%', marginTop: 20 }}>
                    <S.OptionTransmision value={"1"}>LISTA PARA SELEÇÃO DO ENVIO DO RELATÓRIO</S.OptionTransmision>
                    {filterUsers()?.map((itens: any) => {
                        return(
                            <>
                            <S.OptionTransmision value={itens.coD_FUNC}>{itens.email}</S.OptionTransmision>
                            </>
                            
                        )
                    })}
                </S.SelectTransmision>
                <ButtonPrimary functionClick={() => setOpenModal(true)} style={{ marginTop: 10 }} text={'Corpo do E-mail'}></ButtonPrimary>
                <ButtonPrimary functionClick={() => sendEmail()} style={{ marginBottom: 40, marginTop: 20 }} text={'Enviar'}></ButtonPrimary>

            </S.CardStreaming>
            {openModal === true ?
                <S.Modal>
                    <S.TextTittle>Assunto</S.TextTittle>
                    <S.InputAssunt value={assunto} onChange={(e) => setAssunto(e.target.value)} style={{ width: '90%', overflowY: 'auto' }}/>
                    <S.TextTittle>Corpo do E-mail</S.TextTittle>
                    <InputTextarea value={value2} onChange={(e) => setValue2(e.target.value)} style={{ width: '90%', overflowY: 'auto' }} rows={10} cols={30} autoResize />
                    <div>
                        <ButtonPrimary functionClick={() => setOpenModal(false)} style={{ marginTop: 20, marginLeft: 10, marginBottom: 20 }} text={'Fechar'}></ButtonPrimary>
                    </div>
                </S.Modal> : null
            }

        </S.Body>
    )
}

export default Transmission;