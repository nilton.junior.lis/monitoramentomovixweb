import styled from "styled-components";



export const Body = styled.div`
    height: 100%;
    width:  80%;
    display: flex;
    flex-direction: row;
    margin-top: 80px;
`
export const CardStreaming = styled.div`
    background-color: white;
    box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-left: 30px;
    z-index: 1;
`
export const SubCardStreaming = styled.div`
    height: 25%;
    width: 90%;
    background-color: #EAEAEA;
    border-radius: 10px;
    margin-top: 30px;
    display: flexbox;
`

export const TextTittle = styled.text`
    font-style: normal;
    font-weight: 700;
    font-size: 20px;
    line-height: 73px;
    color: #00539B;

`
export const TextSubTittle = styled.text`
    font-style: normal;
    font-weight: 700;
    font-size: 20px;
    line-height: 73px;
    color: #00539B;

`

export const InformateText = styled.text`
    font-style: normal;
    font-size: 15px;
    line-height: 73px;
    color: black;
    text-align: center;
    margin-top: 45%;

`


export const SelectTransmision = styled.select`
    border: 1px solid #00539B;
    height: 40px;
    border-radius: 8px;
    margin: 5px;
    font-size: 13px;
    color: #00539B;
    font-weight: 600;

`

export const InputAssunt = styled.input`
    border: 1px solid grey;
    height: 40px;
    border-radius: 8px;
    margin: 5px;
    font-size: 13px;
    color: "black";

`
export const OptionTransmision = styled.option`
    font-size: 15px;

`

export const Modal = styled.div`
    max-height: 80%;
    width: 50%;
    background-color: white;
    position: absolute;
    z-index: 3;
    margin-top: 10%;
    box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.25);
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    overflow-y: auto;
`
