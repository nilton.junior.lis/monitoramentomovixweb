const Config: any = () => {    
    let env
    fetch('./env.json')
            .then(response => {
                return response.json();
            }).then(data => {
                console.log("data", data);
                env = data
            }).catch((e: Error) => {
                console.log(e.message);
            });
    return env;
};

export default Config();