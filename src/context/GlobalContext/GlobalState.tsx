import { elements } from "chart.js";
import React, { CSSProperties, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import GlobalStateContext from "./GlobalStateContext";
import { api, buildEnv } from "../../service/api";


interface regionalType {
    id: number,
    name: string,
    subsidiaries: filialType
}

interface filialType {
    idRegionalSubsidiary: string,
    id: number,
    name: string    
}


const GlobalState:React.FC<any> = (props:any) => {
    const [result, setResult] = useState([]);        
    const [regional, setRegional] = useState<any>([]);
    const [filial, setFilial] = useState<any>([]);
    const [services , setServices] = useState<any>([]);
    const [unauthorized, setUnauthorized] = useState(false);
    const [regionalSelect, setRegionSelect] = useState<regionalType>();        
    const [config, setConfig] = useState<any>(null);
    const [isAuth, setIsAuth] = useState(false);
    const [inAuth, setInAuth] = useState(true);


    const buildApi = async () => {
        if (config == null)
            buildEnv( setConfig);
    }

    const findFilial = () => {
       let result =  regional.find((element: { id: any }) => element.id == regionalSelect);
       if(result){
        setFilial(result.subsidiaries)
       }else{

       }
    }

    const getSelectData = () => {
        api(config).get('/Regional/FilterCalled?idregional=1',)
        .then((res:any) => {
            setResult(res.data.data)
        })
        .catch((err:any) => {
            console.log(err)
        })

    }

    const getRegions = () => {
        api(config).get('/transmission/subsidiaries')        
        .then((res:any) => {
            setRegional(res.data.data)
        })
        .catch((err:any) => {
            console.log(err)
    })
    }

    const getServices = () => {
        api(config).get('/services')
        .then((res:any) => {
            setServices(res.data.data)
        })
        .catch((err:any) => {
            console.log(err)
        })
    }

    const serviceArray = () => {
        let newArray:any = [];


        services?.map((itens:any) => {
            if(newArray.find((element: { name: any; }) => element.name === itens.name)){
                
            } else {
                newArray.push(itens)
            }
        })
        return newArray
    }

    useEffect(() => {
        buildApi()
        if(config!=null && isAuth) {
            getRegions();
            getServices();
        }
    },[config, isAuth])

    useEffect(() => {
        setFilial(regionalSelect?.subsidiaries)
        console.log(regional)
        findFilial()
    },[regionalSelect])

    const datas:any = {
        regional,
        filial,
        services,
        serviceArray,
        setUnauthorized,
        unauthorized,
        setRegionSelect,
        regionalSelect,
        config,
        isAuth,
        inAuth,
        setIsAuth,
        setInAuth
    }


    return (
        <>
      <GlobalStateContext.Provider value={datas}>
            {props.children}
        </GlobalStateContext.Provider>
        </>
    )
}

export default GlobalState;