import React from "react";
import styled from "styled-components";
import logo from '../assets/imgs/brinkslogo.png';
import iconMonitoramentoWhite from "../assets/icons/monitoramentowhite.png"
import iconMonitoramento from "../assets/icons/monitoramento.png"
import jobs from "../assets/icons/jobs.png";
import jobswhite from "../assets/icons/jobswhite.png";
import streamingwhite from "../assets/icons/streamingwhite.png";
import streaming from "../assets/icons/streaming.png";
import relatorioWhite from "../assets/icons/relatorioswhite.png";
import relatorio from "../assets/icons/relatorio.png";
import dashboard from "../assets/icons/dashboard.png";
import dashboardwhite from "../assets/icons/dashboardwhite.png";


const Body = styled.div`
    width: 20%;
    background-color: #00539B;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-bottom: 140px;
`
const CointainerButtons = styled.div`
    margin-top: 30%;
    width: 80%;
    display: flex;
    flex-direction: column;
    margin-left: 20px;
`

const ButtonsMenu = styled.button`
    background-color: transparent;
    border: none;
    font-size: 20px;
    display: flex;
    flex-direction: row;
    align-items: center;
    height: 60px;
    margin-top: 20px;

    :hover{
        border: 1px solid #42DADA;
        border-radius: 10px;
    }
`

interface MenuProps {
    setSelectOption: Function
    selectOption: number
}



const Menu: React.FC<MenuProps> = ({ selectOption, setSelectOption }) => {


    return (
        <Body>
            <div style={{ display: 'flex', flexDirection: "column", alignItems: "center", padding: 10, backgroundColor: "white", margin: 20, borderRadius: 10 }}>
                <img src={logo} style={{ width: '80%' }}></img>
            </div>
            <CointainerButtons>
                <ButtonsMenu onClick={() => setSelectOption(1)} style={{ color: selectOption === 1 ? "#42DADA" : "white" }}>
                    {selectOption === 1 ? <img style={{ height: "40%", marginRight: 10 }} src={iconMonitoramento}></img> : <img style={{ height: "40%", marginRight: 10 }} src={iconMonitoramentoWhite}></img>}
                    Monitoramento
                </ButtonsMenu>
                <ButtonsMenu onClick={() => setSelectOption(2)} style={{ color: selectOption === 2 ? "#42DADA" : "white" }}>
                    {selectOption === 2 ? <img style={{ height: "40%", marginRight: 10 }} src={jobs}></img> : <img style={{ height: "50%", marginRight: 10 }} src={jobswhite}></img>}
                    Jobs
                </ButtonsMenu>
                <ButtonsMenu onClick={() => setSelectOption(3)} style={{ color: selectOption === 3 ? "#42DADA" : "white" }}>
                    {selectOption === 3 ? <img style={{ height: "30%", marginRight: 10 }} src={streaming}></img> : <img style={{ height: "30%", marginRight: 10 }} src={streamingwhite}></img>}
                    Transmissão
                </ButtonsMenu>
                <ButtonsMenu onClick={() => setSelectOption(4)} style={{ color: selectOption === 4 ? "#42DADA" : "white" }}>
                    {selectOption === 4 ? <img style={{ height: "40%", marginRight: 10 }} src={relatorio}></img> : <img style={{ height: "40%", marginRight: 10 }} src={relatorioWhite}></img>}
                    Relatórios
                </ButtonsMenu>
                {/* <ButtonsMenu onClick={() => setSelectOption(5)} style={{ color: selectOption === 5 ? "#42DADA" : "white" }}>
                    {selectOption === 5 ? <img style={{ height: "30%", marginRight: 10 }} src={dashboard}></img> : <img style={{ height: "30%", marginRight: 10 }} src={dashboardwhite}></img>}
                    DashBoard
                </ButtonsMenu> */}
                <ButtonsMenu onClick={() => setSelectOption(6)} style={{ color: selectOption === 6 ? "#42DADA" : "white" }}>
                    {selectOption === 6 ? <img style={{ height: "30%", marginRight: 10 }} src={streaming}></img> : <img style={{ height: "30%", marginRight: 10 }} src={streamingwhite}></img>}
                    Etapas
                </ButtonsMenu>
            </CointainerButtons>
        </Body>
    )
}

export default Menu;