import React from "react";
import styled from "styled-components";

const Body = styled.button`
    height: 40px;
    width: 150px;
    border: none;
    background-color: #00539B;
    border-radius: 10px;
    color: white;
    :hover{
        background-color: #c4c4c4;
    }
`


interface ButtonPrimaryProps {
    text: string
    style: object
    functionClick: Function
}


const ButtonPrimary: React.FC<ButtonPrimaryProps> = ({ text, style, functionClick }) => {


    return (
        <Body onClick={() => functionClick()} style={style}>
            {text}
        </Body>
    )
}

export default ButtonPrimary;