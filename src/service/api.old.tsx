import axios from 'axios';
import Config from "../Config"

const baseURL = ""//Config().REACT_APP_API_MOVIX;
const api = axios.create({
    headers: {
      'Content-Type': 'application/json'
    },
    baseURL: baseURL,
  });

  api.interceptors.request.use(async (config) => {
    const accessToken =  localStorage.getItem('@token');
  
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
  
    return config;
  });

export default api;