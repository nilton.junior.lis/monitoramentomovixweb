import axios from 'axios';

const api = (config:any) => {
    const api = axios.create({
        headers: {
          'Content-Type': 'application/json'
        },
        baseURL: config.REACT_APP_API_MOVIX,
      });
    
      api.interceptors.request.use(async (config) => {
        const accessToken =  localStorage.getItem('@token');
      
        if (accessToken) {
          config.headers.Authorization = `Bearer ${accessToken}`;
        }
      
        return config;
      });

    return api;
}

const buildEnv = async (setConfig:any) => {  
    (await fetch('/env.json')
          .then(response => {
              return response.json();
          }).then(data => {
              console.log('data', data)              
              setConfig(data)
              
          }).catch((e: Error) => {                
              console.log(e.message);
          }));
}

export {api, buildEnv};